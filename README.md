[![Lines of Code](https://sonarcloud.io/api/project_badges/measure?project=PBSA_peerplays-ethereum&metric=ncloc)](https://sonarcloud.io/summary/new_code?id=PBSA_peerplays-ethereum)
[![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=PBSA_peerplays-ethereum&metric=reliability_rating)](https://sonarcloud.io/summary/new_code?id=PBSA_peerplays-ethereum)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=PBSA_peerplays-ethereum&metric=coverage)](https://sonarcloud.io/summary/new_code?id=PBSA_peerplays-ethereum)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=PBSA_peerplays-ethereum&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=PBSA_peerplays-ethereum)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=PBSA_peerplays-ethereum&metric=bugs)](https://sonarcloud.io/summary/new_code?id=PBSA_peerplays-ethereum)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=PBSA_peerplays-ethereum&metric=vulnerabilities)](https://sonarcloud.io/summary/new_code?id=PBSA_peerplays-ethereum)


# SON for Ethereum Primary wallet smart contract

## Preparing for deployment
```
# Install dependencies
npm install

# Run tests
npx hardhat test

# Run code coverage tests
npx hardhat coverage --network localhost

# Code coverage test will create file coverage/index.html
# where code coverage results can be inspected in details
```

## Building and deploying

Make sure you are running Ethereum node from Peerplays QA Environment.

Unlocking deployment account:
```
# Connect to running Ethereum node
docker exec -it peerplays-qa-environment_ethereum-for-peerplays_1 /bin/bash

# Unlock deployment account for 20 minutes
./geth --exec 'personal.unlockAccount("0x5fbbb31be52608d2f52247e8400b7fcaa9e0bc12", "", 1200)' attach ./network/geth.ipc
```

Deploy smart contract:
```
npx hardhat compile
npx hardhat run scripts/deploy.js --network localhost

# Your output will be similar
Deploying contracts with the account: 0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12
Account balance: 500834000000000000000000
PrimaryWallet address: 0x35C442a810358AFcE6a2743fb685C7b6E6BD7275

# Make note of these two
Deploying contracts with the account: 0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12
PrimaryWallet address: 0x35C442a810358AFcE6a2743fb685C7b6E6BD7275
```

## Geth CLI manual tests
```
# From previous step, we know that our deployment account is
# 0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12
# and that our contract is deployed at address
# 0x35C442a810358AFcE6a2743fb685C7b6E6BD7275

# Connect to running Ethereum node
docker exec -it peerplays-qa-environment_ethereum-for-peerplays_1 /bin/bash

# Unlock deployment account for 20 minutes
./geth --exec 'personal.unlockAccount("0x5fbbb31be52608d2f52247e8400b7fcaa9e0bc12", "", 1200)' attach ./network/geth.ipc

# Get deployment account balance
> eth.getBalance("0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12")
5.01661e+23

# Get primary wallet balance
> eth.getBalance("0x35C442a810358AFcE6a2743fb685C7b6E6BD7275")
0

# Send some funds to primary wallet
> eth.sendTransaction({from: "0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12",to: "0x35C442a810358AFcE6a2743fb685C7b6E6BD7275", value: "1000000000000000000"})
"0xf98f2b0e6ce9fe834e4fde08c6c98dc45a70c9bc6082b54809862a1efd7b6a7d"
> eth.sendTransaction({from: "0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12",to: "0x35C442a810358AFcE6a2743fb685C7b6E6BD7275", value: "1000000000000000000"})
"0xd0d16bdf9ef4d94bcdf3d94ee7a8dc5dab3c2c9209b295dbdacc8f8a37f8b20b"
> eth.sendTransaction({from: "0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12",to: "0x35C442a810358AFcE6a2743fb685C7b6E6BD7275", value: "1000000000000000000"})
"0x1264bd8bdd2d3b442545c8098fb18403e5727292352a0ba19b56aad8baba518f"
> eth.sendTransaction({from: "0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12",to: "0x35C442a810358AFcE6a2743fb685C7b6E6BD7275", value: "1000000000000000000"})
"0x4b0f1bfe361e90fae126dd4f74273c3cab56e7b51f7dfae041d3c8d796c7d0e5"
> eth.sendTransaction({from: "0x5FbBb31BE52608D2F52247E8400B7fCaA9E0bC12",to: "0x35C442a810358AFcE6a2743fb685C7b6E6BD7275", value: "1000000000000000000"})
"0x68207500c028a85ab546a8e55f8b47028a56e227cfb690fa04460ea42962ee34"

# Get primary wallet balance
> eth.getBalance("0x35C442a810358AFcE6a2743fb685C7b6E6BD7275")
5000000000000000000
```

## Useful links

https://hardhat.org/tutorial/creating-a-new-hardhat-project

https://betterprogramming.pub/the-complete-hands-on-hardhat-tutorial-9e23728fc8a4

https://jaredstauffer.medium.com/how-to-implement-basic-multisig-in-ethereum-via-smart-contracts-multi-party-consensus-f1f7eeab36a6

https://docs.soliditylang.org/en/v0.8.15/introduction-to-smart-contracts.html
