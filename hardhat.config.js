require('@nomiclabs/hardhat-ethers');
require("@nomicfoundation/hardhat-toolbox");
require('@openzeppelin/hardhat-upgrades');
require("solidity-coverage");

/**
 * @type import('hardhat/config').HardhatUserConfig
 */
module.exports = {
    defaultNetwork: "hardhat",
    networks: {
        hardhat: {
        },
        localhost: {
            url: "http://localhost:8545",
            accounts: [
                "0xeb5749a569e6141a3b08249d4a0d84f9ef22c67651ba29adb8eb6fd43fc83060",
                "0x77061148cc772e1aa4bc186487d76f41db5744bbf87210a5ebb65d419672c0aa",
                "0x450a08d157e45497506d1745fb3c38c6696a25fe718194915f998bd9c2fa8e45",
                "0x100c34b3c214185f773acd4968f4e146598f7de7d875ddd7e5a8d16254ec5bb7",
                "0xa31a0d461f50de2511d63c9ebfb1d69c51c58355dd7dd098371d2aac3eb7cdbe",
                "0x4f5dd880ff06ff48086dc71d7f27f185f902696ef95b8f6968da56504e829956",
                "0xc3d39eddbe54a76f306b73d018fe8586a75d1fc4c40268e4629fabb712b719e2",
                "0xf165f6dcdaa2794b98037ba019d20192eff7fe9003c4f111add0c4703dba45fc",
                "0x185431915159f97b926e15cad4e73f7680c14f2a5cb788f2f6803832ad37a47c",
                "0xe5db9fa335ccf9ffaa6eea680a64afb941864d6852b31e50644f3906ecda5113",
                "0x804fc14166abb300c25060f82d79fe5afe1eae07c3ceb425272b00d8e2bf7b38",
                "0xee834cec9852dfe37a654d068f5e0f331191f4c8e68d487e04693cd7f9dbd092",
                "0x6eba189f0b02bce534fb6b53b46cee44c53b58317b3020675af7cb7e8d42c1db",
                "0xf9f9adc75a0c5a22f94ba0adcb155df4b1c75ce178d247342d1d2e5473edbd30",
                "0xbd8ca3cbc4d0f4c0acc4b331df1ff3652edfaa6e35f9aa39c88cdaba81f463b1",
                "0x61c0712913c8eefcbad55486726ec5d03f3c4f05742dbd9ff5ab0bacc3597a30",
                "0x4de8ffd3248932ffe2821570153e5eed8bdd6e0e9c2254bdf4aaccaab6bd281e",
                "0x165a8ef53299e5027fc126f94d5de0e073564326216685b604fb1d88b06d5538",
                "0x7f3363e5b81a923432d6bf1dee7611a5fd8ae8f13855d02115ae7b94bfb5a719",
                "0xd59464d5ebd7496c8c78e9bdebd533e387e92743fc6d0aa051405ee41bcf3873",
                "0x50b0569264b5732060af7ac6968e7d0375c19bc23e799cd542bfbcce1b482286",
                "0x85fff31dcbb29823fc6df129c0883210bbf3ada401c4633433baf77c0e73f05a",
                "0xcb83948503d74ee917b42f98efc3b682aafb518e82a15d118a9a3dbdabd00413",
                "0xd615388e8ff47f60534dcd9fae35201027736e5063ad04c973fd221c8c8b9514",
                "0xec23056765966a0ea4fcd7f7e9b84e6f91a275f29ee5039287f553fb6e592141",
                "0x9c1ce812749020c2184959c0ee7b066d0f8b94466aed5288afc789250ff739cf",
                "0x4fd97889702fe893038b7d5c13cc37220b02d2dae2b399a2bb1533095b2916fd",
                "0x6b398d4ca7c2278781a7d3feff8ceaa024f0c909069cfb497c62e0e95aca2bb5",
                "0x275fb51711302c23a3820b4399c1dd0c286b243902c20af67414e2c6a01a5698",
                "0xfc0d8d867c49f11bf3721f8826c22d65e34558318b0ae232d82d31185504e83f",
                "0xbae2512643e67953da8fe614b603e0343495d5d4be8a5f8beb2554069c3866e2",
                "0x902a819d89c054ca325b9eea4b16e3974f8f5e8b06464834ce7823482b3c255e"
            ]
        },
        moonbeam: {
            url: "http://localhost:9933",
            accounts: [
                "0xeb5749a569e6141a3b08249d4a0d84f9ef22c67651ba29adb8eb6fd43fc83060",
                "0x77061148cc772e1aa4bc186487d76f41db5744bbf87210a5ebb65d419672c0aa",
                "0x450a08d157e45497506d1745fb3c38c6696a25fe718194915f998bd9c2fa8e45",
                "0x100c34b3c214185f773acd4968f4e146598f7de7d875ddd7e5a8d16254ec5bb7",
                "0xa31a0d461f50de2511d63c9ebfb1d69c51c58355dd7dd098371d2aac3eb7cdbe",
                "0x4f5dd880ff06ff48086dc71d7f27f185f902696ef95b8f6968da56504e829956",
                "0xc3d39eddbe54a76f306b73d018fe8586a75d1fc4c40268e4629fabb712b719e2",
                "0xf165f6dcdaa2794b98037ba019d20192eff7fe9003c4f111add0c4703dba45fc",
                "0x185431915159f97b926e15cad4e73f7680c14f2a5cb788f2f6803832ad37a47c",
                "0xe5db9fa335ccf9ffaa6eea680a64afb941864d6852b31e50644f3906ecda5113",
                "0x804fc14166abb300c25060f82d79fe5afe1eae07c3ceb425272b00d8e2bf7b38",
                "0xee834cec9852dfe37a654d068f5e0f331191f4c8e68d487e04693cd7f9dbd092",
                "0x6eba189f0b02bce534fb6b53b46cee44c53b58317b3020675af7cb7e8d42c1db",
                "0xf9f9adc75a0c5a22f94ba0adcb155df4b1c75ce178d247342d1d2e5473edbd30",
                "0xbd8ca3cbc4d0f4c0acc4b331df1ff3652edfaa6e35f9aa39c88cdaba81f463b1",
                "0x61c0712913c8eefcbad55486726ec5d03f3c4f05742dbd9ff5ab0bacc3597a30",
                "0x4de8ffd3248932ffe2821570153e5eed8bdd6e0e9c2254bdf4aaccaab6bd281e",
                "0x165a8ef53299e5027fc126f94d5de0e073564326216685b604fb1d88b06d5538",
                "0x7f3363e5b81a923432d6bf1dee7611a5fd8ae8f13855d02115ae7b94bfb5a719",
                "0xd59464d5ebd7496c8c78e9bdebd533e387e92743fc6d0aa051405ee41bcf3873",
                "0x50b0569264b5732060af7ac6968e7d0375c19bc23e799cd542bfbcce1b482286",
                "0x85fff31dcbb29823fc6df129c0883210bbf3ada401c4633433baf77c0e73f05a",
                "0xcb83948503d74ee917b42f98efc3b682aafb518e82a15d118a9a3dbdabd00413",
                "0xd615388e8ff47f60534dcd9fae35201027736e5063ad04c973fd221c8c8b9514",
                "0xec23056765966a0ea4fcd7f7e9b84e6f91a275f29ee5039287f553fb6e592141",
                "0x9c1ce812749020c2184959c0ee7b066d0f8b94466aed5288afc789250ff739cf",
                "0x4fd97889702fe893038b7d5c13cc37220b02d2dae2b399a2bb1533095b2916fd",
                "0x6b398d4ca7c2278781a7d3feff8ceaa024f0c909069cfb497c62e0e95aca2bb5",
                "0x275fb51711302c23a3820b4399c1dd0c286b243902c20af67414e2c6a01a5698",
                "0xfc0d8d867c49f11bf3721f8826c22d65e34558318b0ae232d82d31185504e83f",
                "0xbae2512643e67953da8fe614b603e0343495d5d4be8a5f8beb2554069c3866e2",
                "0x902a819d89c054ca325b9eea4b16e3974f8f5e8b06464834ce7823482b3c255e"
            ]
        },
        sepolia: {
            url: "https://rpc.sepolia.org/",
            accounts: [
                "0xeb5749a569e6141a3b08249d4a0d84f9ef22c67651ba29adb8eb6fd43fc83060",
                "0x77061148cc772e1aa4bc186487d76f41db5744bbf87210a5ebb65d419672c0aa",
                "0x450a08d157e45497506d1745fb3c38c6696a25fe718194915f998bd9c2fa8e45",
                "0x100c34b3c214185f773acd4968f4e146598f7de7d875ddd7e5a8d16254ec5bb7",
                "0xa31a0d461f50de2511d63c9ebfb1d69c51c58355dd7dd098371d2aac3eb7cdbe",
                "0x4f5dd880ff06ff48086dc71d7f27f185f902696ef95b8f6968da56504e829956",
                "0xc3d39eddbe54a76f306b73d018fe8586a75d1fc4c40268e4629fabb712b719e2",
                "0xf165f6dcdaa2794b98037ba019d20192eff7fe9003c4f111add0c4703dba45fc",
                "0x185431915159f97b926e15cad4e73f7680c14f2a5cb788f2f6803832ad37a47c",
                "0xe5db9fa335ccf9ffaa6eea680a64afb941864d6852b31e50644f3906ecda5113",
                "0x804fc14166abb300c25060f82d79fe5afe1eae07c3ceb425272b00d8e2bf7b38",
                "0xee834cec9852dfe37a654d068f5e0f331191f4c8e68d487e04693cd7f9dbd092",
                "0x6eba189f0b02bce534fb6b53b46cee44c53b58317b3020675af7cb7e8d42c1db",
                "0xf9f9adc75a0c5a22f94ba0adcb155df4b1c75ce178d247342d1d2e5473edbd30",
                "0xbd8ca3cbc4d0f4c0acc4b331df1ff3652edfaa6e35f9aa39c88cdaba81f463b1",
                "0x61c0712913c8eefcbad55486726ec5d03f3c4f05742dbd9ff5ab0bacc3597a30",
                "0x4de8ffd3248932ffe2821570153e5eed8bdd6e0e9c2254bdf4aaccaab6bd281e",
                "0x165a8ef53299e5027fc126f94d5de0e073564326216685b604fb1d88b06d5538",
                "0x7f3363e5b81a923432d6bf1dee7611a5fd8ae8f13855d02115ae7b94bfb5a719",
                "0xd59464d5ebd7496c8c78e9bdebd533e387e92743fc6d0aa051405ee41bcf3873",
                "0x50b0569264b5732060af7ac6968e7d0375c19bc23e799cd542bfbcce1b482286",
                "0x85fff31dcbb29823fc6df129c0883210bbf3ada401c4633433baf77c0e73f05a",
                "0xcb83948503d74ee917b42f98efc3b682aafb518e82a15d118a9a3dbdabd00413",
                "0xd615388e8ff47f60534dcd9fae35201027736e5063ad04c973fd221c8c8b9514",
                "0xec23056765966a0ea4fcd7f7e9b84e6f91a275f29ee5039287f553fb6e592141",
                "0x9c1ce812749020c2184959c0ee7b066d0f8b94466aed5288afc789250ff739cf",
                "0x4fd97889702fe893038b7d5c13cc37220b02d2dae2b399a2bb1533095b2916fd",
                "0x6b398d4ca7c2278781a7d3feff8ceaa024f0c909069cfb497c62e0e95aca2bb5",
                "0x275fb51711302c23a3820b4399c1dd0c286b243902c20af67414e2c6a01a5698",
                "0xfc0d8d867c49f11bf3721f8826c22d65e34558318b0ae232d82d31185504e83f",
                "0xbae2512643e67953da8fe614b603e0343495d5d4be8a5f8beb2554069c3866e2",
                "0x902a819d89c054ca325b9eea4b16e3974f8f5e8b06464834ce7823482b3c255e"
            ]
        }
    },
    solidity: "0.8.9"
};
